package com.liuzy.family.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateKit {

	private static SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

	public static String format(Date date) {
		if (date != null) {
			return df.format(date);
		}
		return "";
	}

	public static Date parse(String date) {
		try {
			return df.parse(date);
		} catch (Exception e) {
			return null;
		}
	}
	
}
