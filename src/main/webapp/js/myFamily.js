define(function(require) {
	var _page = 1;// 当前所在页
	var _page_onlyone = false;//当前页只有一条数据
	var req_data = [];
	var del_target_id = 0;
	var status = '';//添加状态、更新状态
	var choseWho = '';//爸爸 妈妈 配偶
	var choseId = '';//已选择人的ID
	var choseName = '';//已选择人的姓名
	var myFamily = {
		person : {},
		init : function(callback) {
			$('#div_person_list').show();
			$('#div_person').hide();
			myFamily.eventBind();
			$('#keywords_list').val('');
			$('#keywords_list')[0].focus();
			myFamily.searchPersonList(1);
		},
		eventBind : function() {
			// 搜索按钮
			$('#btn_search_list').click(function() {
				myFamily.searchPersonList(1);
			});
			// 添加按钮
			$('#btn_add').click(function() {
				status = 'add';
				myFamily.person = {};
				$('#div_person_list').hide();
				$('#div_person').show();
				$('#btn_reset').click();//重置表单
				$('#name')[0].focus();
				var today = new Date();
				$('#yyear').val(today.getFullYear());
				$('#ymonth').val(today.getMonth()+1);
				$('#yday').val(today.getDate());
			});
			// 返回列表
			$('#btn_back').click(function() {
				$('#div_person_list').show();
				$('#div_person').hide();
				$('#keywords_list').val('');
				$('#keywords_list')[0].focus();
				myFamily.searchPersonList(_page);
			});
			$('.nl_div').hide();
			$('#yl_radio').click(function() {
				$('.yl_div').show();
				$('.nl_div').hide();
			});
			$('#nl_radio').click(function() {
				$('.nl_div').show();
				$('.yl_div').hide();
			});
			// 查找模态窗点确认
			$('#btn_ok').click(function() {
				$('#myModal').modal('hide');
				switch (choseWho) {
				case "babaId":
					myFamily.person.babaId = choseId;
					$('#babaId').val(choseName);
					break;
				case "mamaId":
					myFamily.person.mamaId = choseId;
					$('#mamaId').val(choseName);
					break;
				case "spouseId":
					myFamily.person.spouseId = choseId;
					$('#spouseId').val(choseName);
					break;
				default:
					break;
				}
			});
			$('#babaId').click(function() {
				choseWho = "babaId";
				var name = $('#name').val();
				if (name != "")
					$('#keywords').val(name.substr(0,1));
				else
					$('#keywords').val('');
				$('#myModalLabel').html('搜索-爸爸');
			});
			$('#mamaId').click(function() {
				choseWho = "mamaId";
				$('#keywords').val('');
				$('#myModalLabel').html('搜索-妈妈');
			});
			$('#spouseId').click(function() {
				choseWho = "spouseId";
				$('#keywords').val('');
				$('#myModalLabel').html('搜索-配偶');
			});
			// 查找模态窗出现之前
			$('#myModal').on('show.bs.modal', function () {
				choseId = "";
				choseName = "";
				$('#div_choseName').html("");
				$('#person_data').html("");
				$('#person_page').html('<li class="disabled"><a href="javascript:;">&laquo;</a></li><li class="disabled"><a href="javascript:;">&raquo;</a></li>');
				$('#name').popover('hide');
				$('#formerName').popover('hide');
				$('#nickName').popover('hide');
				$('#birthday').popover('hide');
			});
			// 查找模态窗完全出现之后
			$('#myModal').on('shown.bs.modal', function () {
				$('#keywords')[0].focus();
				myFamily.searchPerson(1);
			});
			// 查找模态窗搜索按钮
			$('#btn_search').click(function() {
				myFamily.searchPerson(1);
			});
			// 保存或更新按钮
			$('#btn_saveOrUpdate').click(function() {
				myFamily.person.name = $('#name').val();
				myFamily.person.formerName = $('#formerName').val();
				myFamily.person.nickName = $('#nickName').val();
				myFamily.person.sex = $('#sex').val();
				myFamily.person.birthday = new Date($('#yyear').val() + '-' + $('#ymonth').val() + '-' + $('#yday').val());
				myFamily.person.bNong = $('#nyear').val() + '-' + $('#nmonth').val() + '-' + $('#nday').val();
				var dd = $('#deathday').val();
				if (dd == null || dd == '') {
					myFamily.person.deathday = null;
				} else {
					myFamily.person.deathday = new Date().setTime($('#deathday').val());
				}
				myFamily.person.dNong = "";
				myFamily.person.dNongStr = "";
				myFamily.person.homeAddress = $('#homeAddress').val();
				myFamily.person.homeTell = $('#homeTell').val();
				myFamily.person.workUnits = $('#workUnits').val();
				myFamily.person.workAddress = $('#workAddress').val();
				myFamily.person.phoneNumber = $('#phoneNumber').val();
				if (myFamily.person.name == "" || myFamily.person.name.length < 2) {
					$('#name')[0].focus();
					return;
				}
				if ($('#birthday').val() == "") {
					$('#birthday')[0].focus();
					return;
				}
				$.ajax({
					type : 'POST',
					url : './person/saveOrUpdate.do',
					data : JSON.stringify(myFamily.person),
					contentType : 'application/json',
					dataType : 'json',
					success : function(req) {
						if (status == 'add') {
							//弹窗询问是否继续添加
							$('#goonModal').modal('show');
						} else if (status == 'update'){
							$('#btn_back').click();
						}
					}
				});
			});
			$('#btn_reset').click(function() {
				if (status == 'add') {
					$('#name').val('');
					$('#formerName').val('');
					$('#nickName').val('');
					$('#birthday').val('')
					var today = new Date();
					$('#yyear').val(today.getFullYear());
					$('#ymonth').val(today.getMonth()+1);
					$('#yday').val(today.getDate());
					$('#homeAddress').val('');
					$('#homeTell').val('');
					$('#workUnits').val('');
					$('#workAddress').val('');
					$('#phoneNumber').val('');
					$('#babaId').val('');
					$('#mamaId').val('');
					$('#spouseId').val('');
					$('#name')[0].focus();
				} else if (status == 'update') {
					myFamily.showPerson();
				}
			});
		},
		// 修改按钮
		modify : function(id) {
			status = 'update';
			if (req_data && req_data.length > 0) {
				for(var i in req_data) {
					var p = req_data[i];
					if (p.id == id) {
						myFamily.person = p;
						myFamily.showPerson();
						break;
					}
				}
			}
		},
		// 显示要修改的人
		showPerson : function() {
			$('#name').val(myFamily.person.name);
			$('#formerName').val(myFamily.person.formerName);
			$('#nickName').val(myFamily.person.nickName);
			$('#sex').val(myFamily.person.sex);
			var b = new Date(myFamily.person.birthday);
			$('#yyear').val(b.getFullYear());
			$('#ymonth').val(b.getMonth() + 1);
			$('#yday').val(b.getDate());
			var nong = myFamily.person.bNong.split('-');
			$('#nyear').val(nong[0]);
			$('#nmonth').val(nong[1]);
			$('#nday').val(nong[2]);
			$('#deathday').val(myFamily.person.deathday);
			$('#homeAddress').val(myFamily.person.homeAddress);
			$('#homeTell').val(myFamily.person.homeTell);
			$('#workUnits').val(myFamily.person.workUnits);
			$('#workAddress').val(myFamily.person.workAddress);
			$('#phoneNumber').val(myFamily.person.phoneNumber);
			$('#babaId').val(myFamily.person.babaName);
			$('#mamaId').val(myFamily.person.mamaName);
			$('#spouseId').val(myFamily.person.spouseName);
			// 显示div
			$('#div_person_list').hide();
			$('#div_person').show();
			loadBrithday();
			$('#name')[0].focus();
		},
		// 搜索和翻页
		searchPersonList : function(page) {
			_page = page;
			$.ajax({
				type : 'POST',
				url : './person/searchList.do',
				data : '&keywords=' + $('#keywords_list').val() + "&page=" + page,
				dataType : 'json',
				success : function(req) {
					req_data = req.data;
					if (req.count == 1) {//当前页是不是只有一条，删除时用到
						_page_onlyone = true;
					} else {
						_page_onlyone = false;
					}
					var dataHtml = '';
					var n = 1;
					for (var i in req.data) {
						var p = req.data[i];
						var b = new Date(p.birthday);
						var bYangStr = b.getFullYear() + '年' + (b.getMonth() + 1) + '月' + b.getDate() + '日';
						var age = new Date().getFullYear() - b.getFullYear();
						var sex = p.sex == 'M' ? '男' : '女';
						if (n == 1) {
							dataHtml += "<tr class='active' ";
							n = 0;
						} else {
							dataHtml += "<tr class='warning' ";
							n = 1;
						}//<td>"+p.id+"</td>
						dataHtml += "onclick='chosePerson(\""+p.id+"\", \""+p.name+"\")'><td>"+p.name+"</td><td>"+sex+"</td><td>"+age+"岁</td><td>"+bYangStr+"</td><td>"+p.bNongStr+"</td><td><a href='tel:"+p.homeTell+"'>"+p.homeTell+"</a></td><td><a href='tel:"+p.phoneNumber+"'>"+p.phoneNumber+"</a></td><td><button class=\"btn btn-sm btn-info\" onclick=\"myFamily.modify('"+p.id+"')\">修改</button><td></tr>";// <button class=\"btn btn-sm btn-info\" data-toggle=\"modal\" data-target=\"#delModal\" onclick=\"myFamily.del('"+p.id+"')\">删除</button></td></tr>";
					}
					$('#person_list_data').html(dataHtml);
					var total = req.totalPage;//总页数
					var width = 7;//显示数字个数
					var start = 1;
					var end = width;
					var left = "";//左边有省略
					var right = "";//右边有省略
					if (total > width) {
						if (page > Math.ceil(width / 2)) {//大于一半时往右移动
							left = "...";
							var move = page - Math.ceil(width / 2);
							start += move;
							end += move;
							if (end > total) {//移动后超过了
								var out = end - total;
								end = total;
								start -= out;
							}
						}
						if (end < total)
							right = "...";
					} else {
						end = total;
					}
					var pageHtml = '';
					if (page == 1) {//如果当前页是第一页
						pageHtml += "<li class=\"disabled\">";
					} else {
						pageHtml += "<li>";
					}
					pageHtml += "<a href=\"javascript:myFamily.searchPersonList("+(page==1 ? page : page-1)+");\">&laquo;</a></li>";
					for (var j = start; j <= end; j++) {
						if (j == page) {//如果是当前页
							pageHtml += "<li class=\"active\">";
						} else {
							pageHtml += "<li>";
						}
						var str = '';
						if (j == start) {
							str = left+j;
						} else if (j == end) {
							str = j+right;
						} else {
							str = j;
						}
						pageHtml += "<a href=\"javascript:myFamily.searchPersonList("+j+");\">"+str+"</a></li>";
					}
					if (page == req.totalPage) {//如果当前页是最后一页
						pageHtml += "<li class=\"disabled\">";
					} else {
						pageHtml += "<li>";
					}
					pageHtml += "<a href=\"javascript:myFamily.searchPersonList("+(page==req.totalPage ? page : page+1)+");\">&raquo;</a></li>";
					$('#person_list_page').html(pageHtml);
				}
			});
		},
		// 删除按钮
		del : function(id) {
			del_target_id = id;
		},
		// 删除提示窗点确定
		dialog_del_ok : function() {
			$.ajax({
				type : 'POST',
				url : './person/del.do',
				data : '&id=' + del_target_id,
				dataType : 'json',
				success : function(req) {
					del_target_id = 0;
					$('#delModal').modal('hide');
					myFamily.searchPersonList((_page_onlyone && _page > 0) ? _page -1 : _page);
				},
				error : function(err) {
					del_target_id = 0;
					$('#delModal').modal('hide');
				}
			});
		},
		// 是否继续提示窗点确定
		dialog_goon_ok : function() {
			$('#goonModal').modal('hide');
			$('#btn_reset').click();//重置表单
			myFamily.person = {};
		},
		// 是否继续提示窗点取消
		dialog_goon_no : function() {
			$('#btn_back').click();//返回列表
		},
		// 记录查找模态窗选择的人
		chosePerson : function(id, name) {
			choseId = id;
			choseName = name;
			$('#div_choseName').html(name);
		},
		// 查找模态窗搜索和翻页
		searchPerson : function(page) {
			var sex = null;
			switch (choseWho) {
			case "babaId":
				sex = 'M'; break;
			case "mamaId":
				sex = 'F'; break;
			case "spouseId":
				sex = $('#sex').val() == 'M' ? 'F' : 'M'; break;
			default:
				break;
			}
			$.ajax({
				type : 'POST',
				url : './person/searchList.do',
				data : '&keywords=' + $('#keywords').val() + '&sex=' + sex + "&page=" + page,
				dataType : 'json',
				success : function(req) {
					var dataHtml = '';
					var n = 1;
					for (var i = 0; i < req.data.length; i++) {
						var p = req.data[i];
						var b = new Date(p.birthday);
						var bYangStr = b.getFullYear() + '年' + (b.getMonth() + 1) + '月' + b.getDate() + '日';
						var age = new Date().getFullYear() - b.getFullYear();
						var sex = p.sex == 'M' ? '男' : '女';
						if (n == 1) {
							dataHtml += "<tr class='active' ";
							n = 0;
						} else {
							dataHtml += "<tr class='warning' ";
							n = 1;
						}
						dataHtml += "onclick='chosePerson(\""+p.id+"\", \""+p.name+"\")'><td>"+p.name+"</td><td>"+sex+"</td><td>"+age+"岁</td><td>"+bYangStr+"</td><td>"+p.bNongStr+"</td></tr>";
					}
					var total = req.totalPage;//总页数
					var width = 7;//显示数字个数
					var start = 1;
					var end = width;
					var left = "";//左边有省略
					var right = "";//右边有省略
					if (total > width) {
						if (page > Math.ceil(width / 2)) {//大于一半时往右移动
							left = "...";
							var move = page - Math.ceil(width / 2);
							start += move;
							end += move;
							if (end > total) {//移动后超过了
								var out = end - total;
								end = total;
								start -= out;
							}
						}
						if (end < total)
							right = "...";
					} else {
						end = total;
					}
					$('#person_data').html(dataHtml);
					var pageHtml = '';
					if (page == 1) {//如果当前页是第一页
						pageHtml += "<li class=\"disabled\">";
					} else {
						pageHtml += "<li>";
					}
					pageHtml += '<a href="javascript:myFamily.searchPerson('+(page-1)+');">&laquo;</a></li>';
					for (var j = start; j <= end; j++) {
						if (j == page) {//如果是当前页
							pageHtml += '<li class="active">';
						} else {
							pageHtml += "<li>";
						}
						var str = '';
						if (j == start) {
							str = left+j;
						} else if (j == end) {
							str = j+right;
						} else {
							str = j;
						}
						pageHtml += '<a href="javascript:myFamily.searchPerson('+j+');">'+str+'</a></li>';
					}
					if (page == req.totalPage) {//如果当前页是最后一页
						pageHtml += '<li class="disabled">';
					} else {
						pageHtml += "<li>";
					}
					pageHtml += '<a href="javascript:myFamily.searchPerson('+(page+1)+');">&raquo;</a></li>';
					$('#person_page').html(pageHtml);
				}
			});
		}
	}
	return myFamily;
});
//显示选择的生日
function loadBrithday() {
	if($('#yl_radio').is(':checked')) {
		$.ajax({
			type : 'POST',
			url : './calendar/byYang.do',
			data : "&date=" + $('#yyear').val() + '-' + $('#ymonth').val() + '-' + $('#yday').val(),
			dataType : 'json',
			success : function(req) {
				myFamily.person.bNongStr = req.nongStr;//记住农历时间
				$('#birthday').val(req.yangStr + " " + req.weekday);
				var date = req.nong.split('-');
				$('#nyear').val(date[0]);
				$('#nmonth').val(date[1]);
				$('#nday').val(date[2]);
			}
		});
	} else {
		$.ajax({
			type : 'POST',
			url : './calendar/byNong.do',
			data : "&date=" + $('#nyear').val() + '-' + $('#nmonth').val() + '-' + $('#nday').val(),
			dataType : 'json',
			success : function(req) {
				$('#birthday').val(req.nongStr);
				myFamily.person.bNongStr = req.nongStr;//记住农历时间
				var date = req.yang.split('-');
				$('#yyear').val(date[0]);
				$('#ymonth').val(date[1]);
				$('#yday').val(date[2]);
			}
		});
	}
}
function chosePerson(id, name) {
	myFamily.chosePerson(id, name);
}
